package common_utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.xssf.usermodel.XSSFCell;
//import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_Data_Reader {
	public static ArrayList<String> Read_Excel_Data(String FileName, String SheetName, String TestCaseName)
			throws IOException {

		ArrayList<String> ArrayData = new ArrayList<String>();

		// Step 1 Locate the file

		String project_Dir = System.getProperty("user.dir");

		FileInputStream fis = new FileInputStream(project_Dir + "\\DataFile\\" + FileName); // throws exception why?--> code to fail if file is not found.
																				
		// Step 2 Access the located excel file

		XSSFWorkbook wb = new XSSFWorkbook(fis); //fis object contains path and file name.

		// Step 3 Count the number of sheets available in given excel sheet.

		int countOfSheet = wb.getNumberOfSheets();
		System.out.println(countOfSheet);
		
		//Step4 Access the desired sheet
		for(int i = 0; i<countOfSheet; i++) {
			String sheet_Name = wb.getSheetName(i);//Returns the sheet name
			if(sheet_Name.equals(SheetName)) {
				System.out.println("Inside the sheet : " + sheet_Name);
				XSSFSheet sheet = wb.getSheetAt(i);//It will return the sheet object,So we have to store it in XSSFSheet object.
				Iterator<Row> Rows = sheet.iterator();//Iterating the rows...Row is child of sheet.
				//Pointer for iterator is always behind the starting point.
				while(Rows.hasNext()) { //hasNext() method will check for next element and return the boolean value.
					Row currentRow = Rows.next();//Method to access the next element.
					
					//Step 5 Access the row corresponding to desired test case
					
					if(currentRow.getCell(0).getStringCellValue().equals(TestCaseName)) {
						Iterator<Cell> Cell = currentRow.iterator();//Cell is child of row.
						while(Cell.hasNext()) {
							String Data = Cell.next().getStringCellValue();
//							System.out.println(Data);
							ArrayData.add(Data);
						
						}
						}
				}
			}
		}
		wb.close();

		return ArrayData;
		


	}
}
//		int rows = sheet.getLastRowNum();
//		int cols = sheet.getRow(1).getLastCellNum();
//
//		for (int r = 0; r <= rows; r++) {
//
//			XSSFRow row = sheet.getRow(r);
//
//			for (int c = 0; c < cols; c++) {
//
//				XSSFCell cell = row.getCell(c);
//
//				System.out.print(cell.getStringCellValue());
//
//				System.out.print(" | ");
//			}
//			System.out.println();
//		}

		

