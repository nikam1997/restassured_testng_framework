package common_utility;


//import java.io.File;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
//import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
//import com.relevantcodes.extentreports.LogStatus;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;


public class ExtentReportListener implements ITestListener { 
	
	public static ExtentReports extent;	
	
    private static ThreadLocal<ExtentTest> test = new ThreadLocal<>();
    
    public void onStart(ITestContext context) {
    	
    	ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("extentReport.html");
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter); 
        
    }
   
    public void onTestStart(ITestResult testName) {
    	
        ExtentTest extentTest = extent.createTest(testName.getName());
        test.set(extentTest);
//        
    }

    public void onTestSuccess(ITestResult result) {
        test.get().pass("Test passed");
    }

    public void onTestFailure(ITestResult result) {
        test.get().fail("Test failed").log(Status.ERROR, result.getThrowable());
    }

    public void onFinish(ITestContext context) {
        extent.flush();
    }
    
	public static ExtentTest getTest() {
		return test.get();

	}

    }
 

