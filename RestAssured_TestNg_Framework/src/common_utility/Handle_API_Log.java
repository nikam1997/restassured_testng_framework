package common_utility;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_API_Log {

	// Objective : Creating directory inside our project

	public static File Create_Log_Directory(String DirectoryName) {

		String Current_project_directory = System.getProperty("user.dir");
		// It will give current project directory.
//		System.out.println(Current_project_directory);

		File Log_Directory = new File(Current_project_directory + "\\API_logs\\",DirectoryName);

		Delete_Directory(Log_Directory);

		Log_Directory.mkdir();// Creating Directory.
//		System.out.println(Log_Directory);
		return Log_Directory;
	}

	public static boolean Delete_Directory(File Directory) {

		boolean Directory_deleted = Directory.delete();

		if (Directory.exists()) {

			File[] files = Directory.listFiles();

			// Get all the names of the files(in array) present in the given directory.

			if (files != null) {

				for (File file : files) { // For each loop

					if (file.isDirectory()) {

						Delete_Directory(file); // Called Recursion/Recursive method -->Calling same method inside the
												// method.
					} else {
						file.delete();
					}
				}
			} else {
				Directory_deleted = Directory.delete();
			}
		} else {
			System.out.println("Directory does not exist.");
		}
		return Directory_deleted;
	}

	public static void evidence_creator(File dirname, String filename, String endpoint, String requestBody,
			String responseBody) throws IOException {
		
		// step1 create the file at given location

		File newfile = new File(dirname + "\\" + filename + ".txt");

		System.out.println("new file created to save evidence : " + newfile.getName()); // newfile.getName() will give
																						// you file(newfile) created.

		// step 2 write data into the file

		FileWriter datawriter = new FileWriter(newfile);
		datawriter.write("End point : " + endpoint + "\n\n");
		datawriter.write("Request Body : \n\n" + requestBody + "\n\n");
		datawriter.write("Response Body: \n\n" + responseBody);
		datawriter.close();
		System.out.println("EVIDENCE IS WRITTEN IN FILE : " + newfile.getName());
		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");

	}
	// Exception Handling --> Exception Handling is a mechanism to handle runtime errors such as RunTimeExceptions, IOException etc.
			// The core advantage of exception handling is to maintain the normal flow of
			// the application. An exception normally disrupts the normal flow of the
			// application; that is why we need to handle exceptions
	public static void get_evidence_creator(File dirname, String filename, String endpoint, String responseBody)
			throws IOException {

		// step1 create the file at given location

		File newfile = new File(dirname + "\\" + filename + ".txt");

		System.out.println("new file created to save evidence : " + newfile.getName());

		// step 2 write data into the file

		FileWriter datawriter = new FileWriter(newfile);
		datawriter.write("End point : " + endpoint + "\n\n");
		datawriter.write("Response Body: \n\n" + responseBody);
		datawriter.close();
		System.out.println("EVIDENCE IS WRITTEN IN FILE : " + newfile.getName());
		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");

	}
public static void delete_evidence_Creator (File dirname,String filename,String endpoint,int statuscode) throws IOException {
		
		File newfile = new File(dirname + "\\" + filename + ".txt");
		
		System.out.println("CREATING EVIDENCE FILE : " + newfile.getName());
		FileWriter data = new FileWriter(newfile);
		
		data.write("postEndpoint : " + endpoint + "\n\n");
		data.write("Status Code : " + statuscode );
		data.close();
		System.out.println("EVIDENCE IS WRITTEN IN FILE : " + newfile.getName());
		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		
	}
	
}
