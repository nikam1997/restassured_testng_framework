package reqRepository;

//import java.io.IOException;
//import java.util.ArrayList;

//import commonUtilityPackage.Excel_Data_Reader;

public class endPoints {
	static String hostname = "https://reqres.in/";

	public static String postendpoint() {
		String postURL = hostname + "api/users";
		
		return postURL;

	}
	public static String patchendpoint() {
		String patchURL = hostname + "api/users/2";
	
		return patchURL;

	}
	public static String putendpoint() {
		String putURL = hostname + "api/users/2";
		
		return putURL;

	}
	public static String getendpoint() {

		String getURL = hostname + "api/users?page=2";
		
		return getURL;

	}
	public static String deleteEndPoint() {
		String deleteURL = hostname + "api/users/2";
		return deleteURL;
	}
}
