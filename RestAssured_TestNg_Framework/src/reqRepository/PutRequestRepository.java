package reqRepository;

import java.io.IOException;
import java.util.ArrayList;

import common_utility.Excel_Data_Reader;

public class PutRequestRepository extends endPoints {
	public static String TC1_request() throws IOException {
		

		ArrayList<String> excelData = Excel_Data_Reader.Read_Excel_Data("API_TestData.xlsx", "Put_API", "Put_TC2");
		System.out.println(excelData);
//
		String req_name = excelData.get(1);
		String req_job = excelData.get(2);


		String requestBody = "{\r\n"
				+ "    \"name\": \""+req_name+"\",\r\n"
				+ "    \"job\": \""+req_job+"\"\r\n"
				+ "}";
		
		return requestBody;
		
//		String requestBody = "{\r\n"
//				+ "    \"name\": \"morpheus\",\r\n"
//				+ "    \"job\": \"zion resident\"\r\n"
//				+ "}";
//		
//		return requestBody;
	}
}
