package testCases;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import commonMethod.Trigger_API_DeleteMethod;
import common_utility.ExtentReportListener;
import common_utility.Handle_API_Log;

public class Delete_TC1 extends Trigger_API_DeleteMethod {
	@Test
		public static void DeleteExecutor() throws IOException {
			File dirName = Handle_API_Log.Create_Log_Directory("Delete_TC");
			String URL = deleteEndPoint();
//			System.out.println(statusCode);
			for (int i = 0; i < 5; i++) {
				int statusCode = Trigger_API_DeleteMethod.extract_SC_Delete(URL);
//				System.out.println("Status Code : " + statusCode);
				 ExtentReportListener.getTest().log(Status.INFO, "Status Code : " + statusCode);

				if (statusCode == 204) {
//				System.out.println(statusCode);
				Handle_API_Log.delete_evidence_Creator(dirName, "Delete_TC", URL, statusCode);

				break;
				} else {
	                ExtentReportListener.getTest().log(Status.INFO, "Desired status code not found hence, retry");

				}
				Assert.assertEquals(statusCode, 201);
			    ExtentReportListener.getTest().log(Status.PASS, "Validation passed for Delete_TC1");

			}
		}

	

}
