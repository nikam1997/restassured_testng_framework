package testCases;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import commonMethod.Trigger_API_PutMethod;
import common_utility.ExtentReportListener;
import common_utility.Handle_API_Log;
import io.restassured.path.json.JsonPath;
import reqRepository.PutRequestRepository;

public class Put_TC1 extends Trigger_API_PutMethod {
	@Test
	public static void PutExecutor() throws IOException {
		 String requestBody = PutRequestRepository.TC1_request();

		File dirname = Handle_API_Log.Create_Log_Directory("Put_tc1");

		for (int i = 0; i < 5; i++) {

			int Status_Code = extract_Status_Code(requestBody, putendpoint());

//			System.out.println("Status Code : " + Status_Code);
			 ExtentReportListener.getTest().log(Status.INFO, "Status Code : " + Status_Code);


			if (Status_Code == 200) {
				String ResponseBody = extract_response_body(requestBody, putendpoint());

//				System.out.println("Response Body : " + ResponseBody);
				ExtentReportListener.getTest().log(Status.INFO, "Response Body : " + ResponseBody);

				Handle_API_Log.evidence_creator(dirname, "Put_tc1", putendpoint(), requestBody, ResponseBody);

				Validator(requestBody,ResponseBody);
				break;

			} else {
                ExtentReportListener.getTest().log(Status.INFO, "Desired status code not found hence, retry");

			}
		}

	}
	
	public static void Validator(String requestBody,String ResponseBody) {

		// Create an object of JsonPath to parse the Response body.

		JsonPath jsp_res = new JsonPath(ResponseBody);

		String res_name = jsp_res.getString("name");

		String res_job = jsp_res.getString("job");


		String res_updatedAt = jsp_res.getString("updatedAt");

		String generatedDate = res_updatedAt.substring(0, 10);

		LocalDateTime CurrentDate = LocalDateTime.now();

		String newDate = CurrentDate.toString();

		String updatedDate = newDate.substring(0, 10);



		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");


		String req_job = jsp_req.getString("job");


		// Validation

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(generatedDate, updatedDate);
	    ExtentReportListener.getTest().log(Status.PASS, "Validation passed for Put_TC1");

	}
}
