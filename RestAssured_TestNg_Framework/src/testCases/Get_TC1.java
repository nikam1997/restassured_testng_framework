package testCases;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import commonMethod.Trigger_API_GetMethod;
import common_utility.ExtentReportListener;
import common_utility.Handle_API_Log;
import io.restassured.path.json.JsonPath;

public class Get_TC1 extends Trigger_API_GetMethod {
	
	@Test
	public static void GetExecutor() throws IOException {
		
		File dirname = Handle_API_Log.Create_Log_Directory("Get_TC1"); 
		String endpoint =getendpoint();
		
		for (int i = 0; i < 5; i++) {
			
			int Status_Code = extract_Status_Code(endpoint);
			
//			System.out.println("Status Code : " + Status_Code);
			 ExtentReportListener.getTest().log(Status.INFO, "Status Code : " + Status_Code);

			
			if (Status_Code == 200) {

				String ResponseBody = extract_response_body(endpoint);
				
//				System.out.println("Response Body : " + ResponseBody);
				 ExtentReportListener.getTest().log(Status.INFO, "Response Body : " + ResponseBody);

				
				Handle_API_Log.get_evidence_creator(dirname, "Get_tc1", endpoint, ResponseBody);

				Validator(ResponseBody);
				
				break;

			} else {
                ExtentReportListener.getTest().log(Status.INFO, "Desired status code not found hence, retry");
			}
		}
	}
	

	public static void Validator(String ResponsBody) {

		String exp_id_array[] = { "7", "8", "9", "10", "11", "12" };
		String exp_email_array[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String exp_first_name_array[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String exp_last_name_array[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String exp_avatar_array[] = { "https://reqres.in/img/faces/7-image.jpg",
				"https://reqres.in/img/faces/8-image.jpg", "https://reqres.in/img/faces/9-image.jpg",
				"https://reqres.in/img/faces/10-image.jpg", "https://reqres.in/img/faces/11-image.jpg",
				"https://reqres.in/img/faces/12-image.jpg" };

		JsonPath jsp_res = new JsonPath(ResponsBody);	
		List<Object> res_data = jsp_res.getList("data");	
		//to store the list of array object from data(responseBody)...List is type of collection.	
		int count = res_data.size();
		//count = 6 			
		for (int i = 0; i < count; i++) {

			String Exp_id = exp_id_array[i];// 7
			String res_id = jsp_res.getString("data[" + i + "].id");
			Assert.assertEquals(res_id, Exp_id);
			
			String Exp_email = exp_email_array[i];
			String res_email = jsp_res.getString("data[" + i + "].email");// data.email[i]...data.email[0],data.email[1]
			Assert.assertEquals(res_email, Exp_email);
			
			String Exp_first_name = exp_first_name_array[i];
			String res_first_name = jsp_res.getString("data[" + i + "].first_name");
			Assert.assertEquals(res_first_name, Exp_first_name);
			
			String Exp_last_name = exp_last_name_array[i];
			String res_last_name = jsp_res.getString("data[" + i + "].last_name");
			Assert.assertEquals(res_last_name, Exp_last_name);
			
			String Exp_avatar = exp_avatar_array[i];
			String res_avatar = jsp_res.getString("data[" + i + "].avatar");		
			Assert.assertEquals(res_avatar, Exp_avatar);

		}
	
		
		String page = jsp_res.getString("page");
		
		String per_page = jsp_res.getString("per_page");
		String total = jsp_res.getString("total");
		String total_pages = jsp_res.getString("total_pages");

		Assert.assertEquals(page, "2");
		Assert.assertEquals(per_page, "6");
		Assert.assertEquals(total, "12");
		Assert.assertEquals(total_pages, "2");
	    ExtentReportListener.getTest().log(Status.PASS, "Validation passed for Get_TC1");


	}
}
