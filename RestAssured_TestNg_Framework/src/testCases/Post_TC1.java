package testCases;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

//import java.time.LocalDateTime;

import org.testng.Assert;
//import org.testng.ITestResult;
//import org.testng.annotations.AfterMethod;
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

//import com.aventstack.extentreports.*;
//import com.aventstack.extentreports.ExtentTest;
//import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import commonMethod.Trigger_API_PostMethod;
import common_utility.ExtentReportListener;
//import common_utility.ExtentReportManager;
import common_utility.Handle_API_Log;
import io.restassured.path.json.JsonPath;
import reqRepository.PostRequestRepository;

public class Post_TC1 extends Trigger_API_PostMethod {
//    @BeforeMethod                        //it is executed before each test method.It sets up the Extent Report using 
//    public void setup() { 
//        ExtentReportListener.extentReportSetup("Post_TC1 Test");  
//    }

	@Test
	public static void PostExecutor() throws IOException {

		String requestBody = PostRequestRepository.TC1_request();

		File dirname = Handle_API_Log.Create_Log_Directory("Post_tc1");

		for (int i = 0; i < 5; i++) {

			String postURL = postendpoint();

			int Status_Code = extract_Status_Code(requestBody, postURL);
			
			 ExtentReportListener.getTest().log(Status.INFO, "Status Code : " + Status_Code);


//			System.out.println("Status Code : " + Status_Code);

			if (Status_Code == 201) {

				String ResponseBody = extract_response_body(requestBody, postURL);

//				System.out.println("Response Body : " + ResponseBody);
				ExtentReportListener.getTest().log(Status.INFO, "Response Body : " + ResponseBody);

				Handle_API_Log.evidence_creator(dirname, "Post_tc1", postURL, requestBody, ResponseBody);

				Validator(requestBody, ResponseBody);
				break;

			} else {
                ExtentReportListener.getTest().log(Status.INFO, "Desired status code not found hence, retry");

			}
		}

	}

	public static void Validator(String requestBody, String ResponseBody) throws IOException {

		// Create an object of JsonPath to parse the Response body.

		JsonPath jsp_res = new JsonPath(ResponseBody);

		String res_name = jsp_res.getString("name");

		String res_job = jsp_res.getString("job");

		int res_id = jsp_res.getInt("id");


		String res_createdAt = jsp_res.getString("createdAt");

		String generatedDate = res_createdAt.substring(0, 10);

		LocalDateTime CurrentDate = LocalDateTime.now();

		String newDate = CurrentDate.toString();

		String updatedDate = newDate.substring(0, 10);


		// object of JsonPath to parse the request body.

		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");


		String req_job = jsp_req.getString("job");


		// Validation

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(generatedDate, updatedDate);
	    ExtentReportListener.getTest().log(Status.PASS, "Validation passed for Post_TC1");

	}

}
