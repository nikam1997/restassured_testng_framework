package commonMethod;

import static io.restassured.RestAssured.given;
import reqRepository.PostRequestRepository;
import reqRepository.endPoints;

public class Trigger_API_DeleteMethod extends endPoints{
	public static int extract_SC_Delete(String URL) {

		int statusCode = given().header("Content-Type", "application/json").when().delete(URL).then().extract()
				.statusCode();
		return statusCode;

	}
}
