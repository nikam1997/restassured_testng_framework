package commonMethod;

import static io.restassured.RestAssured.given;

import reqRepository.endPoints;

public class Trigger_API_GetMethod extends endPoints {

	public static int extract_Status_Code(String URL) {

		int StatusCode = given().when().get(URL).then().extract().statusCode();

		return StatusCode;
	}

	public static String extract_response_body(String URL) {

		String ResponseBody = given().when().get(URL).then().extract().response().asString();

		return ResponseBody;

	}
}
