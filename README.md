# RestAssured_TestNG_Framework

This Framework is a Systematic and structure for develoing and running automated tests for RESTful API's such as Post,Put,Patch,Get and Delete by using the java,maven,testNG and rest Assured.
Main Objective is, It provides flexibility,reusabilty,maintainability and readability of code and can automate multiple test cases at one time.
So,basically this framework contains the different packages as below and inside this package it contains api specific classes.

1. Common_utility_package: (Contains common utility classes)
2. Common_method_package: (Includes common methods)
3. request_repository_package: (Includes API-specific request repository classes)
4. test_case_package: (Contains test case classes)

## Let's take an example for PostAPI request 

Inside the request_repository_package I have created "Endpoints.java" and "Post_Request_Repository.java" class which contains the API specific methods.In "Post_Request_Repository.java" class I have created method which is TC1_request() which contains the API specific request body.and to pass the data insted of hard coded data, I have used Data_Driven_testing to fetch the data from Excel file and used this data inside the request body by creating ArrayList.for that,I have created "Excel_data_Reader.java" class inside the Common_utility_package which will used to read the data from excel file.
After that I have created class "Trigger_Post_APIMethod.java" inside the Common_method_package and which contains the methods to extract status Code and Response body which will send the request with endpoint and request body parameters which is inherited from the classes "Endpoints.java" and "Post_Request_Repository.java".
For the execution of test cases and for validation of responses, I have created "Post_TC1.java" class inside the test_case_package which contains the executor() and Validator() methods. 
For running the test cases we convert the project into testNG and execute the test cases via testNG.xml file.

## Allure and Extent Report

This project uses both Allure Report and Extent Report for test reporting. To generate reports:

For Allure Report, make sure you've added the required dependency, run your tests, generate results, and view reports with the provided commands.

For Extent Report,make sure you've added the required dependency, set up by adding the ExtentReportListener class. Configure it in the testNG.xml file by specifying the listener in the <suite> element. Afterward, run your tests, and access the reports from the path mentioned in your project directory. 



